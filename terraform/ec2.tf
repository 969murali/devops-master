resource "aws_instance" "bastion" {
  ami           = ${data.aws_ami.amazon_linux}
  instance_type = var.instance_type
}

resource "aws_instance" "web" {
  ami           = ami-046dd95b1255a4a03
  instance_type = var.instance_type
}

tags = {
  author = "mm"
  Environment = "dev"
}
